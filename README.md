## Standalone Header/Footer
This is meant to help 3rd party sites comply as much as possible with ASU Web Standards 2.0.

### Images
These are all the images used in the new header/footer. Edit the HTML and CSS as needed to point to the images in your framework.
### Omeka
The header stylesheet for Omeka

### Springshare
There are two stylesheets because I was attempting to separate some basic bootstrap styling vs WS2.0 but I might have been making extra work. Feel free to combine these if you can.

This header has no menu at the top until a decision is made about menus for all SpringShare sites. Until then, none of them should have a menu.

NOTE: The SpringShare header does not have a mobile menu. I was having a really hard time getting the mobile menu to work with existing SpringShare styling so I gave it up as a bad job for now. We can come back to this later but for now, I removed all of the mobile menu code.

**libguides-css-js-20.html** contains all of the code from the current "Custom JS/CSS Code" tab in the Look & Feel section of LibGuides. I've edited some lines between the shortcut icons and the body declaration but the rest I didn't touch.

I had been importing the CSS files here from another site to help with testing but I removed those import statements in favor of local uploads.

Some more of that can probably be removed dealing with some of the old footer styling but I didn't want to break anything.

**libguides-header-20.html** should replace everything in the Page Header section of "Header / Footer / Tabs / Boxes" tab in Look & Feel.

**static-footer-with-libraryfoot.html** should replace everything in the Page Footer section of "Header / Footer / Tabs / Boxes" tab in Look & Feel.

### ILLiad
I've included these files in hopes they can help as well. The ILLiad include-header.html file has an example of a header with a menu as well as the login link. 

If your 3rd party site doesn't use SSO, you probably should not include that login link.
